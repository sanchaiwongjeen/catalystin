﻿using System;
using Microsoft.EntityFrameworkCore;
using API.Core.Entity;
using API.Core.Interface;

namespace API.Infrastructure
{
    public class ApiContext : DbContext, IApiContext
    {
        public ApiContext(DbContextOptions<ApiContext> options) : base(options)
        {
        }
        public DbSet<catalystdetail> catalystdetails { get; set; }
        public DbSet<catalyst> catalysts { get; set; }
        public DbSet<catalystINFO> catalystINFOs { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<catalyst>(e =>
            {
                e.ToTable("catalyst");
                e.HasKey(k => k.id);
            });
            modelBuilder.Entity<catalystdetail>(e => {
                e.ToTable("catalystdetail");
                e.HasKey(k => k.id);
                //e.HasOne(s => s.Catalyst).WithMany(m => m.CatalystDetails).HasForeignKey(i => i.uid);
            });
        }
    }
}

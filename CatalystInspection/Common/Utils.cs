﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatalystInspection.Common
{
    public class Utils
    {
        public static Dictionary<int, string> Months
        {
            get
            {
                return new Dictionary<int, string>()
                {
                    { 1, "Overlap" },
                    { 2, "Surfacetop" },
                    { 3, "Surfaceside" }
                };

            }
        }
    }
}

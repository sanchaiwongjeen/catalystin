﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatalystInspection.Models
{
    public class RestCatalystViewModel
    {
        public string uid { get; set; }
        public string defect_type { get; set; }
        public string imagebase64 { get; set; }
        public string image_name { get; set; }
        public DateTime date_input { get; set; }
    }
}

﻿using API.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatalystInspection.Models
{
    public class CatalystImgViewModel
    {
        public CatalystImgViewModel()
        {
            imgoverlap = new List<catalystdetail>();
            imgsurtop = new List<catalystdetail>();
            imgsurside = new List<catalystdetail>();
        }
        public IEnumerable<catalystdetail> imgoverlap { get; set; }
        public IEnumerable<catalystdetail> imgsurtop { get; set; }
        public IEnumerable<catalystdetail> imgsurside { get; set; }
    }
}

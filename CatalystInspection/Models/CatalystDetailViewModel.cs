﻿using API.Core.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatalystInspection.Models
{
    public class CatalystDetailViewModel
    {
        public CatalystDetailViewModel()
        {
            catINFO = new List<catalystINFO>();
        }
        
        public string datefrom { get; set; }
        public string dateto { get; set; }
        public IEnumerable<catalystINFO> catINFO { get; set; }

    }
}

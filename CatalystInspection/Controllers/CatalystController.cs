﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using API.Core.Entity;
using API.Core.Interface;
using ImageMagick;
using System.IO;
using CatalystInspection.Models;

namespace CatalystInspection.Controllers
{
    public class CatalystController : Controller
    {
        public readonly ICatalystServieces _Catalyst;
        public CatalystController(ICatalystServieces catalyst)
        {
            _Catalyst = catalyst;
        }
        public IActionResult Index(CatalystDetailViewModel dv)
        {

            string path = @"wwwroot";
            string fullPath;
            string current;

            fullPath = Path.GetFullPath(path);
            current = Directory.GetCurrentDirectory();
            string pathSave = $"wwwroot{"/images/imgcatalyst/"}";
            var path_ = Path.Combine(Directory.GetCurrentDirectory(), pathSave, "aa" + ".png");
            IEnumerable<catalystINFO> cInfo = new List<catalystINFO>();
            //ViewData["datefrom"] = DateTime.Now.ToString("dd/MM/yyyy");
            //ViewData["dateto"] = DateTime.Now.ToString("dd/MM/yyyy");
            var tmp = _Catalyst.GetCatalystINFO();
            var res = tmp.Select(o => new catalystINFO { 
                date_input = DateTime.Parse(o.date_input.ToString())
            });
            
            if (!String.IsNullOrEmpty(dv.datefrom) && !String.IsNullOrEmpty(dv.dateto))
            {
                DateTime dateF = DateTime.Parse(dv.datefrom + " 0:00:00");
                DateTime dateT = DateTime.Parse(dv.dateto + " 23:59:59");
                cInfo = tmp.Where(o => DateTime.Parse(o.date_input.ToString()) >= dateF && DateTime.Parse(o.date_input.ToString()) <= dateT);
            }
            else
            {
                cInfo = tmp;
            }

            //var d = tmp.Select(o => new { a = o.overlap.ToString() });
            CatalystDetailViewModel catalystView = new CatalystDetailViewModel();
            catalystView.catINFO = cInfo;
            
            return View(catalystView);
        }
        public async Task<IActionResult> DetailInfo(string uid)
        {
            CatalystImgViewModel cImg = new CatalystImgViewModel();
            var tmp = await _Catalyst.GetCatalydtDetailByID(uid);
            var resoverlap = tmp.Where(o => o.defect_type == "0").ToList();
            var ressurtop = tmp.Where(o => o.defect_type == "1").ToList();
            var ressurside = tmp.Where(o => o.defect_type == "2").ToList();
            cImg.imgoverlap = resoverlap;
            cImg.imgsurtop = ressurtop;
            cImg.imgsurside = ressurside;
            return View(cImg);
        }
        
        [HttpGet]
        public async Task<IActionResult> NewCatalyst(string timestamp)
        {
            try
            {
                catalyst cl = new catalyst();
                cl.date_input = Convert.ToDateTime(DateTime.Now.ToString());
                cl.timestamp = timestamp;
                await _Catalyst.Addcatalyst(cl);
                return Ok();
            }catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
        [HttpPost]
        public async Task<IActionResult> NewCatalystDetail([FromBody]RestCatalystViewModel restdata)
        {
            try
            {
                SaveimageBase64(restdata.imagebase64, restdata.defect_type +"_"+ restdata.image_name, 416, 416);
                catalystdetail cd = new catalystdetail();
                cd.uid = restdata.uid;
                cd.defect_type = restdata.defect_type;
                cd.image = restdata.defect_type + "_" + restdata.image_name;
                cd.date_input = Convert.ToDateTime(DateTime.Now.ToString());
                await _Catalyst.AddDetailCatalyst(cd);
                return Ok();
            }catch(Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

       
        public void SaveimageBase64(string data,string filename, int maxWidth, int maxHeight)
        {
            try
            {
                var bytes = Convert.FromBase64String(data);
                var image = new MagickImage(bytes);
                string pathSave = $"wwwroot{"/images/imgcatalyst/"}";
                var path_ = Path.Combine(Directory.GetCurrentDirectory(), pathSave, filename+".png");
                var size = new MagickGeometry(maxWidth, maxHeight);
                image.Resize(size);
                image.Write(path_);
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                Console.WriteLine("Message Error on method SaveimageBase64 " + error);
            }
        }

        
    }
}
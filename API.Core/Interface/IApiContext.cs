﻿using API.Core.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace API.Core.Interface
{
   public interface IApiContext 
    {
        //DbSet<transection> Transections { get; set; }
        //DbSet<line> lines { get; set; }
        DbSet<catalystdetail> catalystdetails { get; set; }
        DbSet<catalyst> catalysts { get; set; }
        DbSet<catalystINFO> catalystINFOs { get; set; }
        int SaveChanges();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
        EntityEntry Entry(object entity);
        EntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
    }
}

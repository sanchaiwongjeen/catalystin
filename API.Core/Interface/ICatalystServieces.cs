﻿using API.Core.Entity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace API.Core.Interface
{
    public interface ICatalystServieces
    {
        Task Addcatalyst(catalyst cl);
        Task AddDetailCatalyst(catalystdetail cd);
        Task<IEnumerable<catalyst>> GetCatalyst();
        IEnumerable<catalystINFO> GetCatalystINFO();
        Task<IEnumerable<catalystdetail>> GetCatalydtDetailByID(string uid);
    }
}

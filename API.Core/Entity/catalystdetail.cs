﻿using System;
using System.Collections.Generic;
using System.Text;

namespace API.Core.Entity
{
    public class catalystdetail
    {
        public int id { get; set; }
        public string uid { get; set; }
        public string defect_type { get; set; }
        public string image { get; set; }
        public DateTime date_input { get; set; }

        //public catalyst Catalyst { get; set; }
    }
}

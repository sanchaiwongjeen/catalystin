﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace API.Core.Entity
{
    public class catalystINFO
    {
        [Key]
        public string uid { get; set; }
        public int overlap { get; set; }
        public int surfacetop { get; set; }
        public int surfaceside { get; set; }
        public DateTime date_input { get; set; }

    }
}

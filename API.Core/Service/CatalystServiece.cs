﻿using API.Core.Entity;
using API.Core.Interface;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Core.Service
{
    public class CatalystServiece : ICatalystServieces
    {
        private readonly IApiContext _Context;
        public CatalystServiece(IApiContext context)
        {
            _Context = context;
        }
        public async Task Addcatalyst(catalyst cl)
        {
            _Context.catalysts.Add(cl);
            await _Context.SaveChangesAsync();

        }
        public async Task AddDetailCatalyst(catalystdetail cd)
        {
            _Context.catalystdetails.Add(cd);
            await _Context.SaveChangesAsync();
        }

        public async Task<IEnumerable<catalyst>> GetCatalyst()
        {
            return await _Context.catalysts.ToListAsync();
        }
        public async Task<IEnumerable<catalystdetail>> GetCatalydtDetailByID(string uid)
        {
            return await _Context.catalystdetails.Where(o => o.uid == uid).ToListAsync();
        }
        public IEnumerable<catalystINFO> GetCatalystINFO()
        {
            IEnumerable<catalystINFO> a = _Context.catalystINFOs.FromSql("select t.*," +
                         "(select COUNT(cd.defect_type) from catalystdetail cd where cd.uid = t.uid and cd.defect_type = '0') overlap," +
                         "(select COUNT(cd.defect_type) from catalystdetail cd where cd.uid = t.uid and cd.defect_type = '1') surfacetop," +
                         "(select COUNT(cd.defect_type) from catalystdetail cd where cd.uid = t.uid and cd.defect_type = '2') surfaceside" +
                         " from(select cd.uid, cl.date_input from catalystdetail cd, catalyst cl where cd.uid = cl.timestamp group by cd.uid, cl.date_input) t").ToList();
            return a;
        }
    }
}
